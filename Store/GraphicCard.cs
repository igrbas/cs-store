﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store {
    public class GraphicCard : Item, IItem {

        public GraphicCard() { }

        public GraphicCard(object id, object price, object manufacturer, object quantity, object picture, object model, object vRam, object cudaCores, object gpuClockSpeed, object ramType) : base(id, price, manufacturer, quantity, picture) {
            Model = (string)model;
            VRam = (int)vRam;
            CudaCores = (int)cudaCores;
            GpuClockSpeed = (double)gpuClockSpeed;
            RamType = (string)ramType;
        }

        public GraphicCard(double price, string manufacturer, int quantity, string picture, string model, int vRam, int cudaCores, double gpuClockSpeed, string ramType) : base(price, manufacturer, quantity, picture) {
            Model = model;
            VRam = vRam;
            CudaCores = cudaCores;
            GpuClockSpeed = gpuClockSpeed;
            RamType = ramType;
        }

        public override string ToString() {
            return String.Format("Graphic card {0} {1}\r\n{2} $", Manufacturer, Model, Price);
        }

        public override string shortInfo() {
            return String.Format("GRAPHIC CARD {0} {1}", Manufacturer, Model).ToUpper();
        }

        public override string Details() {
            return String.Format("Price: {0}$\r\nQuantity: {1}\r\nManufacutrer: {2}\r\nModel: {3}\r\nVRAM: {4} GB\r\nCuda Cores: {5}\r\nGPU Clock Speed: {6}\r\nRAM Type: {7}", Price, Quantity, Manufacturer, Model, VRam, CudaCores, GpuClockSpeed, RamType);
        }

        public string Model {
            get {
                return model;
            }
            set {
                if (value.Length > 0) {
                    model = value;
                } else {
                    throw new ItemDataException("Model can't be empty!");
                }
            }
        }

        public int VRam {
            get {
                return vRam;
            }
            set {
                if (value > 0) {
                    vRam = value;
                } else {
                    throw new ItemDataException("Video ram can't be negative!");
                }
            }
        }

        public int CudaCores {
            get {
                return cudaCores;
            }
            set {
                if (value > 0) {
                    cudaCores = value;
                } else {
                    throw new ItemDataException("Cuda cores can't be negative");
                }
            }
        }

        public double GpuClockSpeed {
            get {
                return gpuClockSpeed;
            }
            set {
                if (value > 0) {
                    gpuClockSpeed = value;
                } else {
                    throw new ItemDataException("GPU clock speed can't be negative!");
                }
            }
        }

        public string RamType {
            get {
                return ramType;
            }
            set {
                if (value.Length > 0) {
                    ramType = value;
                } else {
                    throw new ItemDataException("Ram type can't be empty!");
                }
            }
        }


        private string model;
        private int vRam;
        private int cudaCores;
        private double gpuClockSpeed;
        private string ramType;
    }

}
