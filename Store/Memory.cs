﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store {
    public class Memory : Item, IItem {

        public Memory() { }

        public Memory(object id, object price, object manufacturer, object quantity, object picture, object capacity, object speed, object type) : base(id, price, manufacturer, quantity, picture) {
            Capacity = (int)capacity;
            Speed = (int)speed;
            Type = (string)type;
        }

        public Memory(double price, string manufacturer, int quantity, string picture, int capacity, int speed, string type) : base(price, manufacturer, quantity, picture) {
            Capacity = capacity;
            Speed = speed;
            Type = type;
        }
        public override string ToString() {
            return String.Format("RAM {0} {1} {2} GB\r\n{3} $", Manufacturer, Type, Capacity, Price);
        }

        public override string shortInfo() {
            return String.Format("RAM {0} {1} {2} GB", Manufacturer, Type, Capacity).ToUpper();
        }

        public override string Details() {
            return String.Format("Price: {0}$\r\nQuantity: {1}\r\nManufacutrer: {2}\r\nCapacity: {3} GB\r\nSpeed: {4} Mhz\r\nType: {5}", Price, Quantity, Manufacturer, Capacity, Speed, Type);
        }

        public int Capacity {
            get {
                return capacity;
            }
            set {
                if (value > 0) {
                    capacity = value;
                } else {
                    throw new ItemDataException("Capacity can't be negative!");
                }
            }
        }

        public int Speed {
            get {
                return speed;
            }
            set {
                if (value > 0) {
                    speed = value;
                } else {
                    throw new ItemDataException("Speed can't be negative");
                }
            }
        }

        public string Type {
            get {
                return type;
            }
            set {
                if (value.Length > 0) {
                    type = value;
                } else {
                    throw new ItemDataException("Type can't be empty!");
                }
            }
        }

        private int capacity;
        private int speed;
        private string type;
    }
}
