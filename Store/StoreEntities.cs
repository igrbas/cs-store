﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;


namespace Store {
    public class StoreEntities : DbContext {

        public StoreEntities() : base ("name=StoreEntities"){ }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Entity<Processor>().ToTable("Processors");
            modelBuilder.Entity<GraphicCard>().ToTable("GraphicCards");
            modelBuilder.Entity<Memory>().ToTable("Memories");
        }

        public DbSet<Item> Items { get; set; }
        public DbSet<GraphicCard> GraphicCards { get; set; }
        public DbSet<Processor> Processors { get; set; }
        public DbSet<Memory> Memories { get; set; }
    }
}
