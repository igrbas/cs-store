﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store {
    public class Items {

        public static List<Item> getItems() {           
            var context = new StoreEntities();
            var items = from i in context.Items select i;
            return items.ToList();
        }

        public static Item getItem(int id) {
            var context = new StoreEntities();
            return context.Items.Find(id);
        }

        public static void addItem(Item item) {
            var context = new StoreEntities();
            context.Items.Add(item);
            context.SaveChanges();
        }
        
        public static void removeItem(int id) {
            var context = new StoreEntities();
            Item item = context.Items.Find(id);
            context.Items.Remove(item);
            context.SaveChanges();
        }

        public static void updateProcessor(Processor updatedProcessor) {
            var context = new StoreEntities();
            Processor processor = context.Processors.Find(updatedProcessor.Id);
            processor.Price = updatedProcessor.Price;
            processor.Manufacturer = updatedProcessor.Manufacturer;
            processor.Quantity = updatedProcessor.Quantity;
            processor.Picture = updatedProcessor.Picture;
            processor.Model = updatedProcessor.Model;
            processor.ClockSpeed = updatedProcessor.ClockSpeed;
            processor.ThreadsNumber = updatedProcessor.ThreadsNumber;
            processor.CoresNumber = updatedProcessor.CoresNumber;
            processor.Cache = updatedProcessor.Cache;
            context.SaveChanges();
        }

        public static void updateGraphicCard(GraphicCard updatedGraphicCard) {
            var context = new StoreEntities();
            GraphicCard graphicCard = context.GraphicCards.Find(updatedGraphicCard.Id);
            graphicCard.Price = updatedGraphicCard.Price;
            graphicCard.Manufacturer = updatedGraphicCard.Manufacturer;
            graphicCard.Quantity = updatedGraphicCard.Quantity;
            graphicCard.Picture = updatedGraphicCard.Picture;
            graphicCard.Model = updatedGraphicCard.Model;
            graphicCard.CudaCores = updatedGraphicCard.CudaCores;
            graphicCard.VRam = updatedGraphicCard.VRam;
            graphicCard.GpuClockSpeed = updatedGraphicCard.GpuClockSpeed;
            graphicCard.RamType = updatedGraphicCard.RamType;
            context.SaveChanges();
        }

        public static void updateMemory(Memory updatedMemory) {
            var context = new StoreEntities();
            Memory memory = context.Memories.Find(updatedMemory.Id);
            memory.Price = updatedMemory.Price;
            memory.Manufacturer = updatedMemory.Manufacturer;
            memory.Quantity = updatedMemory.Quantity;
            memory.Picture = updatedMemory.Picture;
            memory.Capacity = updatedMemory.Capacity;
            memory.Speed = updatedMemory.Speed;
            memory.Type = updatedMemory.Type;
            context.SaveChanges();
        }
    }
}
