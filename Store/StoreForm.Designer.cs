﻿
namespace Store {
    partial class StoreForm {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.pnlSidebar = new System.Windows.Forms.Panel();
            this.btnAppearance = new System.Windows.Forms.Button();
            this.btnAddMemory = new System.Windows.Forms.Button();
            this.btnAddGraphicCard = new System.Windows.Forms.Button();
            this.btnAddProcessor = new System.Windows.Forms.Button();
            this.btnMemories = new System.Windows.Forms.Button();
            this.btnGraphicCards = new System.Windows.Forms.Button();
            this.btnProcessors = new System.Windows.Forms.Button();
            this.lblMenu = new System.Windows.Forms.Label();
            this.btnItems = new System.Windows.Forms.Button();
            this.pnlNavbar = new System.Windows.Forms.Panel();
            this.lblSearch = new System.Windows.Forms.Label();
            this.tbSearchItem = new System.Windows.Forms.TextBox();
            this.flpItems = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlSidebar.SuspendLayout();
            this.pnlNavbar.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSidebar
            // 
            this.pnlSidebar.AutoScroll = true;
            this.pnlSidebar.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlSidebar.Controls.Add(this.btnAppearance);
            this.pnlSidebar.Controls.Add(this.btnAddMemory);
            this.pnlSidebar.Controls.Add(this.btnAddGraphicCard);
            this.pnlSidebar.Controls.Add(this.btnAddProcessor);
            this.pnlSidebar.Controls.Add(this.btnMemories);
            this.pnlSidebar.Controls.Add(this.btnGraphicCards);
            this.pnlSidebar.Controls.Add(this.btnProcessors);
            this.pnlSidebar.Controls.Add(this.lblMenu);
            this.pnlSidebar.Controls.Add(this.btnItems);
            this.pnlSidebar.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlSidebar.Location = new System.Drawing.Point(0, 0);
            this.pnlSidebar.Name = "pnlSidebar";
            this.pnlSidebar.Size = new System.Drawing.Size(164, 586);
            this.pnlSidebar.TabIndex = 0;
            // 
            // btnAppearance
            // 
            this.btnAppearance.FlatAppearance.BorderSize = 0;
            this.btnAppearance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAppearance.Location = new System.Drawing.Point(0, 500);
            this.btnAppearance.Name = "btnAppearance";
            this.btnAppearance.Size = new System.Drawing.Size(164, 36);
            this.btnAppearance.TabIndex = 9;
            this.btnAppearance.Text = "Appearance";
            this.btnAppearance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAppearance.UseVisualStyleBackColor = true;
            this.btnAppearance.Click += new System.EventHandler(this.btnAppearance_Click);
            // 
            // btnAddMemory
            // 
            this.btnAddMemory.FlatAppearance.BorderSize = 0;
            this.btnAddMemory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddMemory.Location = new System.Drawing.Point(0, 345);
            this.btnAddMemory.Name = "btnAddMemory";
            this.btnAddMemory.Size = new System.Drawing.Size(164, 36);
            this.btnAddMemory.TabIndex = 8;
            this.btnAddMemory.Text = "Add Memory";
            this.btnAddMemory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddMemory.UseVisualStyleBackColor = true;
            this.btnAddMemory.Click += new System.EventHandler(this.btnAddMemory_Click);
            // 
            // btnAddGraphicCard
            // 
            this.btnAddGraphicCard.FlatAppearance.BorderSize = 0;
            this.btnAddGraphicCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddGraphicCard.Location = new System.Drawing.Point(0, 309);
            this.btnAddGraphicCard.Name = "btnAddGraphicCard";
            this.btnAddGraphicCard.Size = new System.Drawing.Size(164, 36);
            this.btnAddGraphicCard.TabIndex = 7;
            this.btnAddGraphicCard.Text = "Add Graphic Card";
            this.btnAddGraphicCard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddGraphicCard.UseVisualStyleBackColor = true;
            this.btnAddGraphicCard.Click += new System.EventHandler(this.btnAddGraphicCard_Click);
            // 
            // btnAddProcessor
            // 
            this.btnAddProcessor.FlatAppearance.BorderSize = 0;
            this.btnAddProcessor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddProcessor.Location = new System.Drawing.Point(0, 273);
            this.btnAddProcessor.Name = "btnAddProcessor";
            this.btnAddProcessor.Size = new System.Drawing.Size(164, 36);
            this.btnAddProcessor.TabIndex = 6;
            this.btnAddProcessor.Text = "Add Processor";
            this.btnAddProcessor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddProcessor.UseVisualStyleBackColor = true;
            this.btnAddProcessor.Click += new System.EventHandler(this.btnAddProcessor_Click);
            // 
            // btnMemories
            // 
            this.btnMemories.FlatAppearance.BorderSize = 0;
            this.btnMemories.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMemories.Location = new System.Drawing.Point(0, 175);
            this.btnMemories.Name = "btnMemories";
            this.btnMemories.Size = new System.Drawing.Size(164, 36);
            this.btnMemories.TabIndex = 5;
            this.btnMemories.Text = "Show Memories";
            this.btnMemories.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMemories.UseVisualStyleBackColor = true;
            this.btnMemories.Click += new System.EventHandler(this.btnMemories_Click);
            // 
            // btnGraphicCards
            // 
            this.btnGraphicCards.FlatAppearance.BorderSize = 0;
            this.btnGraphicCards.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGraphicCards.Location = new System.Drawing.Point(0, 141);
            this.btnGraphicCards.Name = "btnGraphicCards";
            this.btnGraphicCards.Size = new System.Drawing.Size(164, 36);
            this.btnGraphicCards.TabIndex = 4;
            this.btnGraphicCards.Text = "Show Graphic Cards";
            this.btnGraphicCards.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGraphicCards.UseVisualStyleBackColor = true;
            this.btnGraphicCards.Click += new System.EventHandler(this.btnGraphicCards_Click);
            // 
            // btnProcessors
            // 
            this.btnProcessors.FlatAppearance.BorderSize = 0;
            this.btnProcessors.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcessors.Location = new System.Drawing.Point(0, 107);
            this.btnProcessors.Name = "btnProcessors";
            this.btnProcessors.Size = new System.Drawing.Size(164, 36);
            this.btnProcessors.TabIndex = 3;
            this.btnProcessors.Text = "Show Processors";
            this.btnProcessors.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProcessors.UseVisualStyleBackColor = true;
            this.btnProcessors.Click += new System.EventHandler(this.btnProcessors_Click);
            // 
            // lblMenu
            // 
            this.lblMenu.AutoSize = true;
            this.lblMenu.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblMenu.Location = new System.Drawing.Point(12, 11);
            this.lblMenu.Name = "lblMenu";
            this.lblMenu.Size = new System.Drawing.Size(85, 37);
            this.lblMenu.TabIndex = 2;
            this.lblMenu.Text = "Menu";
            this.lblMenu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnItems
            // 
            this.btnItems.FlatAppearance.BorderSize = 0;
            this.btnItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItems.Location = new System.Drawing.Point(0, 74);
            this.btnItems.Name = "btnItems";
            this.btnItems.Size = new System.Drawing.Size(164, 36);
            this.btnItems.TabIndex = 0;
            this.btnItems.Text = "Show Items";
            this.btnItems.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnItems.UseVisualStyleBackColor = true;
            this.btnItems.Click += new System.EventHandler(this.btnItems_Click);
            // 
            // pnlNavbar
            // 
            this.pnlNavbar.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlNavbar.Controls.Add(this.lblSearch);
            this.pnlNavbar.Controls.Add(this.tbSearchItem);
            this.pnlNavbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlNavbar.Location = new System.Drawing.Point(164, 0);
            this.pnlNavbar.Name = "pnlNavbar";
            this.pnlNavbar.Size = new System.Drawing.Size(926, 68);
            this.pnlNavbar.TabIndex = 2;
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSearch.Location = new System.Drawing.Point(6, 18);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(74, 28);
            this.lblSearch.TabIndex = 1;
            this.lblSearch.Text = "Search:";
            // 
            // tbSearchItem
            // 
            this.tbSearchItem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearchItem.BackColor = System.Drawing.Color.White;
            this.tbSearchItem.Location = new System.Drawing.Point(86, 23);
            this.tbSearchItem.Name = "tbSearchItem";
            this.tbSearchItem.Size = new System.Drawing.Size(828, 23);
            this.tbSearchItem.TabIndex = 0;
            this.tbSearchItem.TextChanged += new System.EventHandler(this.tbSearchItem_TextChanged);
            // 
            // flpItems
            // 
            this.flpItems.AutoScroll = true;
            this.flpItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpItems.Location = new System.Drawing.Point(164, 68);
            this.flpItems.Name = "flpItems";
            this.flpItems.Size = new System.Drawing.Size(926, 518);
            this.flpItems.TabIndex = 3;
            // 
            // StoreForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 586);
            this.Controls.Add(this.flpItems);
            this.Controls.Add(this.pnlNavbar);
            this.Controls.Add(this.pnlSidebar);
            this.Name = "StoreForm";
            this.Text = "Store";            
            this.pnlSidebar.ResumeLayout(false);
            this.pnlSidebar.PerformLayout();
            this.pnlNavbar.ResumeLayout(false);
            this.pnlNavbar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSidebar;
        private System.Windows.Forms.Button btnItems;
        private System.Windows.Forms.Label lblMenu;
        private System.Windows.Forms.Button btnProcessors;
        private System.Windows.Forms.Button btnGraphicCards;
        private System.Windows.Forms.Button btnMemories;
        private System.Windows.Forms.Panel pnlNavbar;
        private System.Windows.Forms.FlowLayoutPanel flpItems;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.TextBox tbSearchItem;
        private System.Windows.Forms.Button btnAddProcessor;
        private System.Windows.Forms.Button btnAddGraphicCard;
        private System.Windows.Forms.Button btnAddMemory;
        private System.Windows.Forms.Button btnAppearance;
    }
}

