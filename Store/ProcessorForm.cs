﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Store {
    public partial class ProcessorForm : Form {
        public ProcessorForm() {
            InitializeComponent();
            processor = null;
        }
        public ProcessorForm(Processor processor) {
            InitializeComponent();
            fillForm(processor);
            this.processor = processor;

        }

        private void fillForm(Processor processor) {
            tbPrice.Text = processor.Price.ToString();
            tbQuantity.Text = processor.Quantity.ToString();
            tbManufacturer.Text = processor.Manufacturer;
            tbPicture.Text = processor.Picture;
            tbModel.Text = processor.Model;
            tbClockSpeed.Text = processor.ClockSpeed.ToString();
            tbThreadsNumber.Text = processor.ThreadsNumber.ToString();
            tbCoresNumber.Text = processor.CoresNumber.ToString();
            tbCache.Text = processor.Cache.ToString();
        }

        private bool CheckInput() {
            try {
                Double.Parse(tbPrice.Text);
            } catch (FormatException) {
                MessageBox.Show("Please insert price as a number!");
                return false;
            }
            try {
                int.Parse(tbQuantity.Text);
            } catch (FormatException) {
                MessageBox.Show("Please insert quantity as a whole number!");
                return false;
            }
            try {
                Double.Parse(tbClockSpeed.Text);
            } catch (FormatException) {
                MessageBox.Show("Please insert clock speed as a number!");
                return false;
            }
            try {
                int.Parse(tbThreadsNumber.Text);
            } catch (FormatException) {
                MessageBox.Show("Please insert threads number as a whole number!");
                return false;
            }
            try {
                int.Parse(tbCoresNumber.Text);
            } catch (FormatException) {
                MessageBox.Show("Please insert cores number as a whole number!");
                return false;
            }
            try {
                Double.Parse(tbCache.Text);
            } catch (FormatException) {
                MessageBox.Show("Please insert cache as a number!");
                return false;
            }
            if(tbPicture.Text == "") {
                MessageBox.Show("Please select picture!");
                return false;
            }
            return true;
        }

        private void UpdateProcessor() {
            if (!CheckInput()) {
                return;
            }
            try {
                processor.Price = Double.Parse(tbPrice.Text);
                processor.Manufacturer = tbManufacturer.Text;
                processor.Quantity = int.Parse(tbQuantity.Text);
                processor.Model = tbModel.Text;
                processor.ClockSpeed = Double.Parse(tbClockSpeed.Text);
                processor.ThreadsNumber = int.Parse(tbThreadsNumber.Text);
                processor.CoresNumber = int.Parse(tbCoresNumber.Text);
                processor.Cache = Double.Parse(tbCache.Text);
                if (processor.Picture != tbPicture.Text) {
                    string copyTo = @"C:\StoreImages\" + DateTime.Now.ToString("dd-mm-yy-hh-mm-ss") + ".png";
                    processor.Picture = copyTo;
                    File.Copy(tbPicture.Text, copyTo);
                }
                Items.updateProcessor(processor);
            } catch (ItemDataException ex) {
                MessageBox.Show(ex.Message);
                return;
            } catch (Exception) {
                MessageBox.Show("Error occurred!");                
            }
            MessageBox.Show("Processor successfully updated!");
            Close();
        }

        private void SaveProcessor() {
            if (!CheckInput()) {
                return;
            }
            try {
                string copyTo = @"C:\StoreImages\" + DateTime.Now.ToString("dd-mm-yy-hh-mm-ss") + ".png";
                Items.addItem(new Processor(Double.Parse(tbPrice.Text), tbManufacturer.Text, int.Parse(tbQuantity.Text), copyTo, tbModel.Text, Double.Parse(tbClockSpeed.Text), int.Parse(tbThreadsNumber.Text), int.Parse(tbCoresNumber.Text), Double.Parse(tbCache.Text)));
                File.Copy(tbPicture.Text, copyTo);
            } catch (ItemDataException ex) {
                MessageBox.Show(ex.Message);
                return;
            } catch (Exception) {
                MessageBox.Show("Error occurred!");
            }
            MessageBox.Show("Processor successfully saved!");
            Close();
        }

        private void btnSaveProcessor_Click(object sender, EventArgs e) {            
            if(processor == null) {
                SaveProcessor();
            } else {
                UpdateProcessor();
            }
        }   
        
        private void btnPicture_Click(object sender, EventArgs e) {
            ofdPicture.Filter = "JPeg Image|*.jpg|Png Image|*.png";
            ofdPicture.Title = "Select an image";
            if (ofdPicture.ShowDialog() == DialogResult.OK) {
                if (ofdPicture.FileName != "") {
                    tbPicture.Text = ofdPicture.FileName;
                }
            }
        }        

        private void ProcessorForm_KeyDown(object sender, KeyEventArgs e) {
            if(e.KeyCode == Keys.Enter) {
                if (processor == null) {
                    SaveProcessor();
                } else {
                    UpdateProcessor();
                }
            }
            if(e.KeyCode == Keys.Escape) {
                Close();
            }
        }

        private Processor processor;
    }
}
