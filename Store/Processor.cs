﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store {
    public class Processor : Item, IItem {

        public Processor() { }

        public Processor(object id, object price, object manufacturer, object quantity, object picture, object model, object clockSpeed, object threadsNumber, object coresNumber, object cache) : base(id, price, manufacturer, quantity, picture) {
            Model = (string)model;
            ClockSpeed = (double)clockSpeed;
            ThreadsNumber = (int)threadsNumber;
            CoresNumber = (int)coresNumber;
            Cache = (double)cache;
        }

        public Processor(double price, string manufacturer, int quantity, string picture, string model, double clockSpeed, int threadsNumber, int coresNumber, double cache) : base(price, manufacturer, quantity, picture) {
            Model = model;
            ClockSpeed = clockSpeed;
            ThreadsNumber = threadsNumber;
            CoresNumber = coresNumber;
            Cache = cache;
        }

        public override string ToString() {
            return String.Format("Processor {0} {1}\r\n{2} $", Manufacturer, Model, Price);
        }

        public override string shortInfo() {
            return String.Format("PROCESSOR {0} {1}", Manufacturer, Model).ToUpper();
        }

        public override string Details() {
            return String.Format("Price: {0}$\r\nQuantity: {1}\r\nManufacutrer: {2}\r\nModel: {3}\r\nClock Speed: {4} GHz\r\nNumer of threads: {5}\r\nNumber of cores: {6}\r\nCache: {7} MB", Price, Quantity, Manufacturer, Model,ClockSpeed,ThreadsNumber,CoresNumber,Cache);
        }

        public string Model {
            get {
                return model;
            }
            set {
                if (value.Length > 0) {
                    model = value;
                } else {
                    throw new ItemDataException("Model can't be empty!");
                }
            }
        }

        public double ClockSpeed {
            get {
                return clockSpeed;
            }
            set {
                if (value > 0) {
                    clockSpeed = value;
                } else {
                    throw new ItemDataException("Clock speed can't be negative!");
                }
            }
        }

        public int ThreadsNumber {
            get {
                return threadsNumber;
            }
            set {
                if (value > 0) {
                    threadsNumber = value;
                } else {
                    throw new ItemDataException("Number of threads can't be negative!");
                }
            }
        }

        public int CoresNumber {
            get {
                return coresNumber;
            }
            set {
                if (value > 0) {
                    coresNumber = value;
                } else {
                    throw new ItemDataException("Number of cores can't be negative!");
                }
            }
        }

        public double Cache {
            get {
                return cache;
            }
            set {
                if (value > 0) {
                    cache = value;
                } else {
                    throw new ItemDataException("Cache can't be negative!");
                }
            }
        }

        private string model;
        private double clockSpeed;
        private int threadsNumber;
        private int coresNumber;
        private double cache;
    }

}
