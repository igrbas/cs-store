﻿
namespace Store {
    partial class GraphicCardForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ofdPicture = new System.Windows.Forms.OpenFileDialog();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.btnSaveGraphicCard = new System.Windows.Forms.Button();
            this.lblRamType = new System.Windows.Forms.Label();
            this.tbRamType = new System.Windows.Forms.TextBox();
            this.lblGpuClockSpeed = new System.Windows.Forms.Label();
            this.tbGpuClockSpeed = new System.Windows.Forms.TextBox();
            this.lblModel = new System.Windows.Forms.Label();
            this.tbModel = new System.Windows.Forms.TextBox();
            this.lblVRam = new System.Windows.Forms.Label();
            this.tbVRam = new System.Windows.Forms.TextBox();
            this.lblCudaCores = new System.Windows.Forms.Label();
            this.tbCudaCores = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.tbPrice = new System.Windows.Forms.TextBox();
            this.btnPicture = new System.Windows.Forms.Button();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.tbPicture = new System.Windows.Forms.TextBox();
            this.tbQuantity = new System.Windows.Forms.TextBox();
            this.lblPicture = new System.Windows.Forms.Label();
            this.lblManufacturer = new System.Windows.Forms.Label();
            this.tbManufacturer = new System.Windows.Forms.TextBox();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.btnSaveGraphicCard);
            this.pnlMain.Controls.Add(this.lblRamType);
            this.pnlMain.Controls.Add(this.tbRamType);
            this.pnlMain.Controls.Add(this.lblGpuClockSpeed);
            this.pnlMain.Controls.Add(this.tbGpuClockSpeed);
            this.pnlMain.Controls.Add(this.lblModel);
            this.pnlMain.Controls.Add(this.tbModel);
            this.pnlMain.Controls.Add(this.lblVRam);
            this.pnlMain.Controls.Add(this.tbVRam);
            this.pnlMain.Controls.Add(this.lblCudaCores);
            this.pnlMain.Controls.Add(this.tbCudaCores);
            this.pnlMain.Controls.Add(this.lblPrice);
            this.pnlMain.Controls.Add(this.tbPrice);
            this.pnlMain.Controls.Add(this.btnPicture);
            this.pnlMain.Controls.Add(this.lblQuantity);
            this.pnlMain.Controls.Add(this.tbPicture);
            this.pnlMain.Controls.Add(this.tbQuantity);
            this.pnlMain.Controls.Add(this.lblPicture);
            this.pnlMain.Controls.Add(this.lblManufacturer);
            this.pnlMain.Controls.Add(this.tbManufacturer);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(631, 540);
            this.pnlMain.TabIndex = 0;
            // 
            // btnSaveGraphicCard
            // 
            this.btnSaveGraphicCard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveGraphicCard.Location = new System.Drawing.Point(17, 477);
            this.btnSaveGraphicCard.Name = "btnSaveGraphicCard";
            this.btnSaveGraphicCard.Size = new System.Drawing.Size(601, 23);
            this.btnSaveGraphicCard.TabIndex = 47;
            this.btnSaveGraphicCard.Text = "Save";
            this.btnSaveGraphicCard.UseVisualStyleBackColor = true;
            this.btnSaveGraphicCard.Click += new System.EventHandler(this.btnSaveGraphicCard_Click);
            // 
            // lblRamType
            // 
            this.lblRamType.AutoSize = true;
            this.lblRamType.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblRamType.Location = new System.Drawing.Point(17, 429);
            this.lblRamType.Name = "lblRamType";
            this.lblRamType.Size = new System.Drawing.Size(103, 28);
            this.lblRamType.TabIndex = 45;
            this.lblRamType.Text = "RAM type:";
            // 
            // tbRamType
            // 
            this.tbRamType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRamType.Location = new System.Drawing.Point(180, 434);
            this.tbRamType.Name = "tbRamType";
            this.tbRamType.Size = new System.Drawing.Size(438, 23);
            this.tbRamType.TabIndex = 46;
            // 
            // lblGpuClockSpeed
            // 
            this.lblGpuClockSpeed.AutoSize = true;
            this.lblGpuClockSpeed.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblGpuClockSpeed.Location = new System.Drawing.Point(17, 379);
            this.lblGpuClockSpeed.Name = "lblGpuClockSpeed";
            this.lblGpuClockSpeed.Size = new System.Drawing.Size(162, 28);
            this.lblGpuClockSpeed.TabIndex = 43;
            this.lblGpuClockSpeed.Text = "GPU clock speed:";
            // 
            // tbGpuClockSpeed
            // 
            this.tbGpuClockSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGpuClockSpeed.Location = new System.Drawing.Point(180, 384);
            this.tbGpuClockSpeed.Name = "tbGpuClockSpeed";
            this.tbGpuClockSpeed.Size = new System.Drawing.Size(438, 23);
            this.tbGpuClockSpeed.TabIndex = 44;
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblModel.Location = new System.Drawing.Point(17, 229);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(73, 28);
            this.lblModel.TabIndex = 37;
            this.lblModel.Text = "Model:";
            // 
            // tbModel
            // 
            this.tbModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbModel.Location = new System.Drawing.Point(180, 234);
            this.tbModel.Name = "tbModel";
            this.tbModel.Size = new System.Drawing.Size(438, 23);
            this.tbModel.TabIndex = 38;
            // 
            // lblVRam
            // 
            this.lblVRam.AutoSize = true;
            this.lblVRam.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblVRam.Location = new System.Drawing.Point(17, 279);
            this.lblVRam.Name = "lblVRam";
            this.lblVRam.Size = new System.Drawing.Size(67, 28);
            this.lblVRam.TabIndex = 39;
            this.lblVRam.Text = "VRam:";
            // 
            // tbVRam
            // 
            this.tbVRam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbVRam.Location = new System.Drawing.Point(180, 284);
            this.tbVRam.Name = "tbVRam";
            this.tbVRam.Size = new System.Drawing.Size(438, 23);
            this.tbVRam.TabIndex = 40;
            // 
            // lblCudaCores
            // 
            this.lblCudaCores.AutoSize = true;
            this.lblCudaCores.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblCudaCores.Location = new System.Drawing.Point(17, 329);
            this.lblCudaCores.Name = "lblCudaCores";
            this.lblCudaCores.Size = new System.Drawing.Size(112, 28);
            this.lblCudaCores.TabIndex = 41;
            this.lblCudaCores.Text = "Cuda cores:";
            // 
            // tbCudaCores
            // 
            this.tbCudaCores.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCudaCores.Location = new System.Drawing.Point(180, 334);
            this.tbCudaCores.Name = "tbCudaCores";
            this.tbCudaCores.Size = new System.Drawing.Size(438, 23);
            this.tbCudaCores.TabIndex = 42;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblPrice.Location = new System.Drawing.Point(17, 29);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(58, 28);
            this.lblPrice.TabIndex = 28;
            this.lblPrice.Text = "Price:";
            // 
            // tbPrice
            // 
            this.tbPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPrice.Location = new System.Drawing.Point(180, 34);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Size = new System.Drawing.Size(438, 23);
            this.tbPrice.TabIndex = 29;
            // 
            // btnPicture
            // 
            this.btnPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPicture.Location = new System.Drawing.Point(431, 184);
            this.btnPicture.Name = "btnPicture";
            this.btnPicture.Size = new System.Drawing.Size(187, 23);
            this.btnPicture.TabIndex = 36;
            this.btnPicture.Text = "Select Picture";
            this.btnPicture.UseVisualStyleBackColor = true;
            this.btnPicture.Click += new System.EventHandler(this.btnPicture_Click);
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblQuantity.Location = new System.Drawing.Point(17, 79);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(97, 28);
            this.lblQuantity.TabIndex = 30;
            this.lblQuantity.Text = "Quantity: ";
            // 
            // tbPicture
            // 
            this.tbPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPicture.Enabled = false;
            this.tbPicture.Location = new System.Drawing.Point(180, 184);
            this.tbPicture.Name = "tbPicture";
            this.tbPicture.Size = new System.Drawing.Size(245, 23);
            this.tbPicture.TabIndex = 35;
            // 
            // tbQuantity
            // 
            this.tbQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbQuantity.Location = new System.Drawing.Point(180, 84);
            this.tbQuantity.Name = "tbQuantity";
            this.tbQuantity.Size = new System.Drawing.Size(438, 23);
            this.tbQuantity.TabIndex = 31;
            // 
            // lblPicture
            // 
            this.lblPicture.AutoSize = true;
            this.lblPicture.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblPicture.Location = new System.Drawing.Point(17, 179);
            this.lblPicture.Name = "lblPicture";
            this.lblPicture.Size = new System.Drawing.Size(76, 28);
            this.lblPicture.TabIndex = 34;
            this.lblPicture.Text = "Picture:";
            // 
            // lblManufacturer
            // 
            this.lblManufacturer.AutoSize = true;
            this.lblManufacturer.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblManufacturer.Location = new System.Drawing.Point(17, 129);
            this.lblManufacturer.Name = "lblManufacturer";
            this.lblManufacturer.Size = new System.Drawing.Size(133, 28);
            this.lblManufacturer.TabIndex = 32;
            this.lblManufacturer.Text = "Manufacturer:";
            // 
            // tbManufacturer
            // 
            this.tbManufacturer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbManufacturer.Location = new System.Drawing.Point(180, 134);
            this.tbManufacturer.Name = "tbManufacturer";
            this.tbManufacturer.Size = new System.Drawing.Size(438, 23);
            this.tbManufacturer.TabIndex = 33;
            // 
            // GraphicCardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 540);
            this.Controls.Add(this.pnlMain);
            this.KeyPreview = true;
            this.Name = "GraphicCardForm";
            this.Text = "Graphic card form";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GraphicCardForm_KeyDown);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog ofdPicture;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Button btnSaveGraphicCard;
        private System.Windows.Forms.Label lblRamType;
        private System.Windows.Forms.TextBox tbRamType;
        private System.Windows.Forms.Label lblGpuClockSpeed;
        private System.Windows.Forms.TextBox tbGpuClockSpeed;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.Label lblVRam;
        private System.Windows.Forms.TextBox tbClockSpeed;
        private System.Windows.Forms.Label lblCudaCores;
        private System.Windows.Forms.TextBox tbCudaCores;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox tbPrice;
        private System.Windows.Forms.Button btnPicture;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.TextBox tbPicture;
        private System.Windows.Forms.TextBox tbQuantity;
        private System.Windows.Forms.Label lblPicture;
        private System.Windows.Forms.Label lblManufacturer;
        private System.Windows.Forms.TextBox tbManufacturer;
        private System.Windows.Forms.TextBox tbVRam;
    }
}