﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Drawing;

namespace Store {
    public class Appearance {

        public Appearance() {
            //ne mogu spremiti color u xml pa onda imam string
            SideBackColor = ColorTranslator.ToHtml(Color.FromArgb(215, 228, 242));
            NavBackColor = ColorTranslator.ToHtml(Color.FromArgb(215, 228, 242));
            SideTextColor = ColorTranslator.ToHtml(Color.Black);
            NavTextColor = ColorTranslator.ToHtml(Color.Black);
            SideFont = "Segoe UI";
            NavFont = "Segoe UI";
        }

        //serijalizacija objekta
        public static void SaveXML(object o, string fileName) {
            XmlSerializer xmlFormat = new XmlSerializer(o.GetType());
            using (Stream fs = new FileStream(fileName,
              FileMode.Create, FileAccess.Write, FileShare.None)) {
                xmlFormat.Serialize(fs, o);
            }
        }

        //deserijalizacija objekta
        public static object ReadXML(string fileName) {
            XmlSerializer xmlFormat = new XmlSerializer(typeof(Appearance));
            using (Stream fStream = File.OpenRead(fileName)) {
                return xmlFormat.Deserialize(fStream);
            }
        }

        //obavještavam da se je appearance save-o (došlo do promjene)
        public void Saved() {
            RefreshAppearance?.Invoke(this);
        }

        public string SideBackColor { get; set; }
        public string NavBackColor { get; set; }
        public string SideTextColor { get; set; }
        public string NavTextColor { get; set; }
        public string SideFont { get; set; }
        public string NavFont { get; set; }

        public delegate void RefreshAppearanceDelegate(object sender);
        public event RefreshAppearanceDelegate RefreshAppearance;
    }
}
