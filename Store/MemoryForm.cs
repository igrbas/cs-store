﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Store {
    public partial class MemoryForm : Form {
        public MemoryForm() {
            InitializeComponent();
            memory = null;
        }

        public MemoryForm(Memory memory) {
            InitializeComponent();
            fillForm(memory);
            this.memory = memory;            
        }

        private void fillForm(Memory memory) {
            tbPrice.Text = memory.Price.ToString();
            tbQuantity.Text = memory.Quantity.ToString();
            tbManufacturer.Text = memory.Manufacturer;
            tbPicture.Text = memory.Picture;
            tbCapacity.Text = memory.Capacity.ToString();
            tbSpeed.Text = memory.Speed.ToString();
            tbType.Text = memory.Type;
        }

        private bool CheckInput() {
            try {
                Double.Parse(tbPrice.Text);
            } catch (FormatException ex) {
                MessageBox.Show("Please insert price as a number!");
                return false;
            }
            try {
                int.Parse(tbQuantity.Text);
            } catch (FormatException ex) {
                MessageBox.Show("Please insert quantity as a whole number!");
                return false;
            }
            try {
                int.Parse(tbCapacity.Text);
            } catch (FormatException ex) {
                MessageBox.Show("Please insert capacity as a number!");
                return false;
            }
            try {
                int.Parse(tbSpeed.Text);
            } catch (FormatException ex) {
                MessageBox.Show("Please insert speed as a whole number!");
                return false;
            }
            if (tbPicture.Text == "") {
                MessageBox.Show("Please select picture!");
                return false;
            }
            return true;
        }

        private void UpdateMemory() {
            if (!CheckInput()) {
                return;
            }
            try {
                memory.Price = Double.Parse(tbPrice.Text); 
                memory.Quantity = int.Parse(tbQuantity.Text);
                memory.Manufacturer = tbManufacturer.Text;
                memory.Capacity = int.Parse(tbCapacity.Text);
                memory.Speed = int.Parse(tbSpeed.Text);
                memory.Type = tbType.Text;
                if (memory.Picture != tbPicture.Text) {
                    string copyTo = @"C:\StoreImages\" + DateTime.Now.ToString("dd-mm-yy-hh-mm-ss") + ".png";
                    memory.Picture = copyTo;
                    File.Copy(tbPicture.Text, copyTo);
                }
                Items.updateMemory(memory);
            } catch (ItemDataException ex) {
                MessageBox.Show(ex.Message);
                return;
            } catch (Exception) {
                MessageBox.Show("Error occurred!");
            }
            MessageBox.Show("Memory successfully updated!");
            Close();
        }

        private void SaveMemory() {
            if (!CheckInput()) {
                return;
            }
            try {
                string copyTo = @"C:\StoreImages\" + DateTime.Now.ToString("dd-mm-yy-hh-mm-ss") + ".png";
                Items.addItem(new Memory(Double.Parse(tbPrice.Text), tbManufacturer.Text, int.Parse(tbQuantity.Text), copyTo, int.Parse(tbCapacity.Text), int.Parse(tbSpeed.Text), tbType.Text));
                File.Copy(tbPicture.Text, copyTo);
            } catch (ItemDataException ex) {
                MessageBox.Show(ex.Message);
                return;
            } catch (Exception) {
                MessageBox.Show("Error occurred!");
            }
            MessageBox.Show("Memory successfully saved!");
            Close();
        }

        private void btnSaveMemory_Click(object sender, EventArgs e) {
            if(memory == null) {
                SaveMemory();
            } else {
                UpdateMemory();
            }
        }

        private void btnPicture_Click(object sender, EventArgs e) {
            ofdPicture.Filter = "JPeg Image|*.jpg|Png Image|*.png";
            ofdPicture.Title = "Select an image";
            if (ofdPicture.ShowDialog() == DialogResult.OK) {
                if (ofdPicture.FileName != "") {
                    tbPicture.Text = ofdPicture.FileName;
                }
            }
        }        

        private void MemoryForm_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                if (memory == null) {
                    SaveMemory();
                } else {
                    UpdateMemory();
                }
            }
            if (e.KeyCode == Keys.Escape) {
                Close();
            }
        }

        private Memory memory;
    }
}
