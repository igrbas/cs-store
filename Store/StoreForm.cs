﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace Store {
    public partial class StoreForm : Form {

        static readonly object sharedLock = new object();
        Appearance appearance;

        public StoreForm() {
            InitializeComponent();
            ShowItems(Items.getItems());
            appearance = new Appearance();
            if (File.Exists(@"./save.xml")) {
                appearance = (Appearance)Appearance.ReadXML(@"./save.xml");
            }
            appearance.RefreshAppearance += Appearance_RefreshAppearance;
            ChangeAppearance();
        }
      
        private void Appearance_RefreshAppearance(object sender) {
            this.BeginInvoke(new MethodInvoker(delegate {                
                ChangeAppearance();
            }));
        }

        //promjena izgleda navbara i sidebara
        private void ChangeAppearance() {
            pnlSidebar.BackColor = ColorTranslator.FromHtml(appearance.SideBackColor);
            pnlNavbar.BackColor = ColorTranslator.FromHtml(appearance.NavBackColor);
            foreach(Control c in pnlSidebar.Controls) {
                c.ForeColor = ColorTranslator.FromHtml(appearance.SideTextColor);
                c.Font = new Font(appearance.SideFont, 9);
            }
            lblMenu.Font = new Font(appearance.SideFont, 25);
            lblSearch.ForeColor = ColorTranslator.FromHtml(appearance.NavTextColor);
            lblSearch.Font = new Font(appearance.NavFont, 15);
        }


        //prikaz itema na formi
        private void ShowItems(List<Item> items) {
            lock (sharedLock) {
                if (InvokeRequired) {
                    flpItems.Invoke((MethodInvoker)delegate {
                        flpItems.Controls.Clear();
                    });
                } else {
                    flpItems.Controls.Clear();
                }                
                //Thread.Sleep(2000);
                foreach (Item i in items) {
                    Panel panel = new Panel();
                    panel.Height = 200;
                    panel.Width = 175;
                    panel.BorderStyle = BorderStyle.FixedSingle;
                    panel.Cursor = Cursors.Hand;

                    PictureBox pictureBox = new PictureBox();
                    pictureBox.Height = 150;
                    pictureBox.ImageLocation = i.Picture;
                    pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    pictureBox.Dock = DockStyle.Top;
                    panel.Controls.Add(pictureBox);

                    Label label = new Label();
                    label.Height = 50;
                    label.Text = i.ToString();
                    label.TextAlign = ContentAlignment.MiddleCenter;
                    label.Dock = DockStyle.Bottom;
                    panel.Controls.Add(label);

                    ContextMenuStrip cms = new ContextMenuStrip();                    
                    ToolStripMenuItem tsmiDetails = new ToolStripMenuItem("Details");
                    tsmiDetails.Click += (sender, EventArgs) => { TsmiDetails_Click(sender, EventArgs, i.Id); };
                    ToolStripMenuItem tsmiUpdate = new ToolStripMenuItem("Update");
                    tsmiUpdate.Click += (sender, EventArgs) => { TsmiUpdate_Click(sender, EventArgs, i); };
                    ToolStripMenuItem tsmiRemove = new ToolStripMenuItem("Remove");
                    tsmiRemove.Click += (sender, EventArgs) => { TsmiRemove_Click(sender, EventArgs, i); };
                    cms.Items.AddRange(new ToolStripMenuItem[] { tsmiDetails, tsmiUpdate, tsmiRemove });
                    panel.ContextMenuStrip = cms;

                    if (InvokeRequired) {
                        flpItems.Invoke((MethodInvoker)delegate {
                            flpItems.Controls.Add(panel);
                        });
                    } else {
                        flpItems.Controls.Add(panel);
                    }                    
                }
            }
        }


        //taskovi za brisanje slike i removanje itema
        private Task DeleteAsync(string path) {
            File.Delete(path);
            return Task.CompletedTask;
        }

        private Task RemoveItemAsync(int id) {
            Items.removeItem(id);
            return Task.CompletedTask;
        }


        //hendlanje ToolStripMenuItem-a
        private async void TsmiRemove_Click(object sender, EventArgs e,Item item) {
            var removeItemTask = RemoveItemAsync(item.Id);
            var deletePictureTask = DeleteAsync(item.Picture);
            await removeItemTask;
            await deletePictureTask;
            MessageBox.Show("Item successfully removed!");            
            ShowItems(Items.getItems());
        }

        private void TsmiUpdate_Click(object sender, EventArgs e,Item item) {
            if (item.GetType() == typeof(Processor)){
                ProcessorForm frm = new ProcessorForm((Processor)item);
                frm.Show();
            } else if(item.GetType() == typeof(GraphicCard)) {
                GraphicCardForm frm = new GraphicCardForm((GraphicCard)item);
                frm.Show();
            } else {
                MemoryForm frm = new MemoryForm((Memory)item);
                frm.Show();
            }
        }

        private void TsmiDetails_Click(object sender, EventArgs e, int id) {
            DetailsForm frm = new DetailsForm(id);
            frm.Show();
        }


        //hendlanje buttona za prikaz itema, pozivanje prikaza u zasebnim dretvama
        private void btnItems_Click(object sender, EventArgs e) {
            tbSearchItem.Clear();
            Thread thread = new Thread(() => ShowItems(Items.getItems()));
            thread.IsBackground = true;
            thread.Start();
        }       

        private void btnProcessors_Click(object sender, EventArgs e) {
            tbSearchItem.Clear();
            Thread thread = new Thread(() => ShowItems(Items.getItems().OfType<Processor>().Cast<Item>().ToList()));
            thread.IsBackground = true;
            thread.Start();               
        }

        private void btnGraphicCards_Click(object sender, EventArgs e) {
            tbSearchItem.Clear();
            Thread thread = new Thread(() => ShowItems(Items.getItems().OfType<GraphicCard>().Cast<Item>().ToList()));
            thread.IsBackground = true;
            thread.Start();            
        }

        private void btnMemories_Click(object sender, EventArgs e) {
            tbSearchItem.Clear();
            Thread thread = new Thread(() => ShowItems(Items.getItems().OfType<Memory>().Cast<Item>().ToList()));
            thread.IsBackground = true;
            thread.Start();
        }
             
        //hendlanje pretrazivanje itema
        private void tbSearchItem_TextChanged(object sender, EventArgs e) {
            Thread thread = new Thread(() => ShowItems((from i in Items.getItems() select i).Where(i => i.shortInfo().Contains(tbSearchItem.Text.ToUpper())).ToList()));
            thread.IsBackground = true;
            thread.Start();
        }

        //hendlanje buttona za dodavanje itema
        private void btnAddProcessor_Click(object sender, EventArgs e) {
            ProcessorForm frm = new ProcessorForm();
            frm.Show();
        }

        private void btnAddGraphicCard_Click(object sender, EventArgs e) {
            GraphicCardForm frm = new GraphicCardForm();
            frm.Show();
        }

        private void btnAddMemory_Click(object sender, EventArgs e) {
            MemoryForm frm = new MemoryForm();
            frm.Show();
        }

        //hendlanje buttona za appearance
        private void btnAppearance_Click(object sender, EventArgs e) {
            AppearanceForm frm = new AppearanceForm(appearance);
            frm.Show();
        }        
    }
}
