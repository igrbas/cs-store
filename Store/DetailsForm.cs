﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Store {
    public partial class DetailsForm : Form {

        public DetailsForm(int id) {
            InitializeComponent();
            Item item = Items.getItem(id);
            UpdateControls(item);
        }

        //popunjavanje forme s podacima od itema
        private void UpdateControls(Item item) {
            lblName.Text = item.shortInfo();
            lblDetails.Text = item.Details();
            pbImage.ImageLocation = item.Picture;
        }

        private void button1_Click(object sender, EventArgs e) {
            Close();
        }
    }
}
