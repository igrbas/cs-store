﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store {
    public abstract class Item {
        public Item() { }

        public Item(object id, object price, object manufacturer, object quantity, object picture) {
            Id = (int)id;
            Price = (double)price;
            Manufacturer = (string)manufacturer;
            Quantity = (int)quantity;
            Picture = (string)picture;
        }

        public Item(double price, string manufacturer, int quantity, string picture) {
            Price = price;
            Manufacturer = manufacturer;
            Quantity = quantity;
            Picture = picture;
        }

        public abstract override string ToString();

        public virtual string shortInfo() {
            return String.Format("{0}", Manufacturer).ToUpper();
        }

        public virtual string Details() {
            return String.Format("Price: {0}$\r\nManufacutrer: {1}\r\nQuantity: {2}\r\n",Price,Manufacturer,Quantity);
        }

        public int Id { get; set; }
        public double Price {
            get {
                return price;
            }
            set {
                if (value >= 0) {
                    price = value;
                } else {
                    throw new ItemDataException("Price can't be negative!");
                }
            }
        }

        public string Manufacturer {
            get {
                return manufacturer;
            }
            set {
                if (value.Length > 0) {
                    manufacturer = value;
                } else {
                    throw new ItemDataException("Manufacturer can't be empty!");
                }
            }
        }
        public int Quantity {
            get {
                return quantity;
            }
            set {
                if (value >= 0) {
                    quantity = value;
                } else {
                    throw new ItemDataException("Quantity can't be negative!");
                }
            }
        }
        public string Picture {
            get {
                return picture;
            }
            set {
                if (value.Length > 0) {
                    picture = value;
                } else {
                    throw new ItemDataException("Picture can't be empty!");
                }
            }
        }

        private double price;
        private string manufacturer;
        private int quantity;
        private string picture;
    }
}
