﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store {
    public class ItemDataException : Exception{
        public ItemDataException() : base() { }
        public ItemDataException(string message) : base(message) { }
    }
}
