﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Store {
    public partial class AppearanceForm : Form {

        Appearance appearance;

        public AppearanceForm(Appearance appearance) {
            InitializeComponent();
            this.appearance = appearance;            
        }


        //kada se forma loada, popuni combo boxeve sa fontovima i postavi trenutne vrijednosti izgleda
        private void AppearanceForm_Load(object sender, EventArgs e) {
            ArrayList fonts = new ArrayList(new InstalledFontCollection().Families);
            foreach (FontFamily f in fonts) {
                cbSideFont.Items.Add(f.Name);
                cbNavFont.Items.Add(f.Name);
            }
            pnlSideBackColor.BackColor = ColorTranslator.FromHtml(appearance.SideBackColor);
            pnlNavColor.BackColor = ColorTranslator.FromHtml(appearance.NavBackColor);
            tbSideTextColor.ForeColor = ColorTranslator.FromHtml(appearance.SideTextColor);
            tbNavTextColor.ForeColor = ColorTranslator.FromHtml(appearance.NavTextColor);
            cbSideFont.Text = appearance.SideFont;
            cbNavFont.Text = appearance.NavFont;
        }


        //hendlanje buttona za odabir nove boje
        private void btnSideBackColor_Click(object sender, EventArgs e) {
            if (colorDialog.ShowDialog() == DialogResult.OK) {
                pnlSideBackColor.BackColor = colorDialog.Color;
                appearance.SideBackColor = ColorTranslator.ToHtml(colorDialog.Color);
            }
        }        

        private void btnSideTextColor_Click(object sender, EventArgs e) {
            if (colorDialog.ShowDialog() == DialogResult.OK) {
                tbSideTextColor.ForeColor = colorDialog.Color;
                appearance.SideTextColor = ColorTranslator.ToHtml(colorDialog.Color);
            }
        }

        private void btnNavBackColor_Click(object sender, EventArgs e) {
            if (colorDialog.ShowDialog() == DialogResult.OK) {
                pnlNavColor.BackColor = colorDialog.Color;
                appearance.NavBackColor = ColorTranslator.ToHtml(colorDialog.Color);                
            }
        }

        private void btnNavTextColor_Click(object sender, EventArgs e) {
            if (colorDialog.ShowDialog() == DialogResult.OK) {
                tbNavTextColor.ForeColor = colorDialog.Color;
                appearance.NavTextColor = ColorTranslator.ToHtml(colorDialog.Color);
            }
        }

        //provjera je li upisan font postoji
        private bool CheckFont(Control cb) {
            var fontsCollection = new InstalledFontCollection();
            foreach (FontFamily f in fontsCollection.Families) {
                if(f.Name == cb.Text) {
                    return true;
                }
            }
            return false;
        }

        //hendlanje save buttona
        private void btnSave_Click(object sender, EventArgs e) {
            if (!CheckFont(cbNavFont)) {
                MessageBox.Show("Navbar font doesn't exist!");
                return;
            }
            if (!CheckFont(cbSideFont)) {
                MessageBox.Show("Sidebar font doesn't exist!");
                return;
            }

            appearance.NavFont = cbNavFont.Text;
            appearance.SideFont = cbSideFont.Text;

            Appearance.SaveXML(appearance, @"./save.xml");
            MessageBox.Show("Appearance successfully saved!");
            appearance.Saved();
            Close();

        }
    }
}
