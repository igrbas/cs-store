﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Store {
    public partial class GraphicCardForm : Form {
        public GraphicCardForm() {
            InitializeComponent();
            graphicCard = null;
        }

        public GraphicCardForm(GraphicCard graphicCard) {
            InitializeComponent();
            fillForm(graphicCard);
            this.graphicCard = graphicCard;

        }

        private void fillForm(GraphicCard graphicCard) {
            tbPrice.Text = graphicCard.Price.ToString();
            tbQuantity.Text = graphicCard.Quantity.ToString();
            tbManufacturer.Text = graphicCard.Manufacturer;
            tbPicture.Text = graphicCard.Picture;
            tbModel.Text = graphicCard.Model;
            tbVRam.Text = graphicCard.VRam.ToString();
            tbCudaCores.Text = graphicCard.CudaCores.ToString();
            tbGpuClockSpeed.Text = graphicCard.GpuClockSpeed.ToString();
            tbRamType.Text = graphicCard.RamType;        
        }

        private bool CheckInput() {
            try {
                Double.Parse(tbPrice.Text);
            } catch (FormatException ex) {
                MessageBox.Show("Please insert price as a number!");
                return false;
            }
            try {
                int.Parse(tbQuantity.Text);
            } catch (FormatException ex) {
                MessageBox.Show("Please insert quantity as a whole number!");
                return false;
            }
            try {
                int.Parse(tbVRam.Text);
            } catch (FormatException ex) {
                MessageBox.Show("Please insert VRam as a whole number!");
                return false;
            }
            try {
                int.Parse(tbCudaCores.Text);
            } catch (FormatException ex) {
                MessageBox.Show("Please insert cuda coures as a whole number!");
                return false;
            }
            try {
                Double.Parse(tbGpuClockSpeed.Text);
            } catch (FormatException ex) {
                MessageBox.Show("Please insert GPU clock speed as a number!");
                return false;
            }
            if (tbPicture.Text == "") {
                MessageBox.Show("Please select picture!");
                return false;
            }
            return true;
        }

        private void UpdateGraphicCard() {
            if (!CheckInput()) {
                return;
            }
            try {
                graphicCard.Price = Double.Parse(tbPrice.Text);
                graphicCard.Manufacturer = tbManufacturer.Text;
                graphicCard.Quantity = int.Parse(tbQuantity.Text);
                graphicCard.Model = tbModel.Text;
                graphicCard.VRam = int.Parse(tbVRam.Text);
                graphicCard.CudaCores = int.Parse(tbCudaCores.Text);
                graphicCard.GpuClockSpeed = Double.Parse(tbGpuClockSpeed.Text);
                graphicCard.RamType = tbRamType.Text;
                if (graphicCard.Picture != tbPicture.Text) {
                    string copyTo = @"C:\StoreImages\" + DateTime.Now.ToString("dd-mm-yy-hh-mm-ss") + ".png";
                    graphicCard.Picture = copyTo;
                    File.Copy(tbPicture.Text, copyTo);
                }
                Items.updateGraphicCard(graphicCard);
            } catch (ItemDataException ex) {
                MessageBox.Show(ex.Message);
                return;
            } catch (Exception) {
                MessageBox.Show("Error occurred!");
            }
            MessageBox.Show("Graphic card successfully updated!");
            Close();
        }

        private void SaveGraphicCard() {
            if (!CheckInput()) {
                return;
            }
            try {
                string copyTo = @"C:\StoreImages\" + DateTime.Now.ToString("dd-mm-yy-hh-mm-ss") + ".png";
                Items.addItem(new GraphicCard(Double.Parse(tbPrice.Text), tbManufacturer.Text, int.Parse(tbQuantity.Text), copyTo, tbModel.Text, int.Parse(tbVRam.Text), int.Parse(tbCudaCores.Text), Double.Parse(tbGpuClockSpeed.Text), tbRamType.Text));
                File.Copy(tbPicture.Text, copyTo);
            } catch (ItemDataException ex) {
                MessageBox.Show(ex.Message);
                return;
            } catch (Exception) {
                MessageBox.Show("Error occurred!");
            }
            MessageBox.Show("Graphic card successfully saved!");
            Close();
        }

        private void btnSaveGraphicCard_Click(object sender, EventArgs e) {
            if(graphicCard == null) {
                SaveGraphicCard();
            } else {
                UpdateGraphicCard();
            }
        }

        private void btnPicture_Click(object sender, EventArgs e) {
            ofdPicture.Filter = "JPeg Image|*.jpg|Png Image|*.png";
            ofdPicture.Title = "Select an image";
            if (ofdPicture.ShowDialog() == DialogResult.OK) {
                if (ofdPicture.FileName != "") {
                    tbPicture.Text = ofdPicture.FileName;
                }
            }
        }        

        private void GraphicCardForm_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                if (graphicCard == null) {
                    SaveGraphicCard();
                } else {
                    UpdateGraphicCard();
                }
            }
            if (e.KeyCode == Keys.Escape) {
                Close();
            }
        }

        private GraphicCard graphicCard;
    }
}
