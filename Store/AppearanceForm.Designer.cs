﻿
namespace Store {
    partial class AppearanceForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlNavColor = new System.Windows.Forms.Panel();
            this.pnlSideBackColor = new System.Windows.Forms.Panel();
            this.cbNavFont = new System.Windows.Forms.ComboBox();
            this.cbSideFont = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblNavTextFont = new System.Windows.Forms.Label();
            this.btnNavTextColor = new System.Windows.Forms.Button();
            this.tbNavTextColor = new System.Windows.Forms.TextBox();
            this.lblNavTextColor = new System.Windows.Forms.Label();
            this.btnNavBackColor = new System.Windows.Forms.Button();
            this.lblNavBackColor = new System.Windows.Forms.Label();
            this.lblNavbar = new System.Windows.Forms.Label();
            this.lblSideTextFont = new System.Windows.Forms.Label();
            this.btnSideTextColor = new System.Windows.Forms.Button();
            this.tbSideTextColor = new System.Windows.Forms.TextBox();
            this.lblSideTextColor = new System.Windows.Forms.Label();
            this.btnSideBackColor = new System.Windows.Forms.Button();
            this.lblSideBackColor = new System.Windows.Forms.Label();
            this.lblSidebar = new System.Windows.Forms.Label();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.AutoScroll = true;
            this.pnlMain.Controls.Add(this.pnlNavColor);
            this.pnlMain.Controls.Add(this.pnlSideBackColor);
            this.pnlMain.Controls.Add(this.cbNavFont);
            this.pnlMain.Controls.Add(this.cbSideFont);
            this.pnlMain.Controls.Add(this.btnSave);
            this.pnlMain.Controls.Add(this.lblNavTextFont);
            this.pnlMain.Controls.Add(this.btnNavTextColor);
            this.pnlMain.Controls.Add(this.tbNavTextColor);
            this.pnlMain.Controls.Add(this.lblNavTextColor);
            this.pnlMain.Controls.Add(this.btnNavBackColor);
            this.pnlMain.Controls.Add(this.lblNavBackColor);
            this.pnlMain.Controls.Add(this.lblNavbar);
            this.pnlMain.Controls.Add(this.lblSideTextFont);
            this.pnlMain.Controls.Add(this.btnSideTextColor);
            this.pnlMain.Controls.Add(this.tbSideTextColor);
            this.pnlMain.Controls.Add(this.lblSideTextColor);
            this.pnlMain.Controls.Add(this.btnSideBackColor);
            this.pnlMain.Controls.Add(this.lblSideBackColor);
            this.pnlMain.Controls.Add(this.lblSidebar);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(634, 461);
            this.pnlMain.TabIndex = 0;
            // 
            // pnlNavColor
            // 
            this.pnlNavColor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlNavColor.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlNavColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNavColor.Location = new System.Drawing.Point(189, 275);
            this.pnlNavColor.Name = "pnlNavColor";
            this.pnlNavColor.Size = new System.Drawing.Size(323, 23);
            this.pnlNavColor.TabIndex = 45;
            // 
            // pnlSideBackColor
            // 
            this.pnlSideBackColor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSideBackColor.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlSideBackColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSideBackColor.Location = new System.Drawing.Point(189, 66);
            this.pnlSideBackColor.Name = "pnlSideBackColor";
            this.pnlSideBackColor.Size = new System.Drawing.Size(323, 23);
            this.pnlSideBackColor.TabIndex = 44;
            // 
            // cbNavFont
            // 
            this.cbNavFont.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbNavFont.FormattingEnabled = true;
            this.cbNavFont.Location = new System.Drawing.Point(189, 375);
            this.cbNavFont.Name = "cbNavFont";
            this.cbNavFont.Size = new System.Drawing.Size(323, 23);
            this.cbNavFont.TabIndex = 43;
            // 
            // cbSideFont
            // 
            this.cbSideFont.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSideFont.FormattingEnabled = true;
            this.cbSideFont.Location = new System.Drawing.Point(189, 170);
            this.cbSideFont.Name = "cbSideFont";
            this.cbSideFont.Size = new System.Drawing.Size(323, 23);
            this.cbSideFont.TabIndex = 42;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(238, 423);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(180, 25);
            this.btnSave.TabIndex = 41;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblNavTextFont
            // 
            this.lblNavTextFont.AutoSize = true;
            this.lblNavTextFont.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNavTextFont.Location = new System.Drawing.Point(48, 377);
            this.lblNavTextFont.Name = "lblNavTextFont";
            this.lblNavTextFont.Size = new System.Drawing.Size(71, 21);
            this.lblNavTextFont.TabIndex = 38;
            this.lblNavTextFont.Text = "Text font:";
            // 
            // btnNavTextColor
            // 
            this.btnNavTextColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNavTextColor.Location = new System.Drawing.Point(518, 325);
            this.btnNavTextColor.Name = "btnNavTextColor";
            this.btnNavTextColor.Size = new System.Drawing.Size(89, 23);
            this.btnNavTextColor.TabIndex = 37;
            this.btnNavTextColor.Text = "Select color";
            this.btnNavTextColor.UseVisualStyleBackColor = true;
            this.btnNavTextColor.Click += new System.EventHandler(this.btnNavTextColor_Click);
            // 
            // tbNavTextColor
            // 
            this.tbNavTextColor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNavTextColor.Location = new System.Drawing.Point(189, 325);
            this.tbNavTextColor.Name = "tbNavTextColor";
            this.tbNavTextColor.Size = new System.Drawing.Size(323, 23);
            this.tbNavTextColor.TabIndex = 36;
            this.tbNavTextColor.Text = "Text color";
            // 
            // lblNavTextColor
            // 
            this.lblNavTextColor.AutoSize = true;
            this.lblNavTextColor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNavTextColor.Location = new System.Drawing.Point(48, 327);
            this.lblNavTextColor.Name = "lblNavTextColor";
            this.lblNavTextColor.Size = new System.Drawing.Size(78, 21);
            this.lblNavTextColor.TabIndex = 35;
            this.lblNavTextColor.Text = "Text color:";
            // 
            // btnNavBackColor
            // 
            this.btnNavBackColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNavBackColor.Location = new System.Drawing.Point(518, 275);
            this.btnNavBackColor.Name = "btnNavBackColor";
            this.btnNavBackColor.Size = new System.Drawing.Size(89, 23);
            this.btnNavBackColor.TabIndex = 34;
            this.btnNavBackColor.Text = "Select color";
            this.btnNavBackColor.UseVisualStyleBackColor = true;
            this.btnNavBackColor.Click += new System.EventHandler(this.btnNavBackColor_Click);
            // 
            // lblNavBackColor
            // 
            this.lblNavBackColor.AutoSize = true;
            this.lblNavBackColor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNavBackColor.Location = new System.Drawing.Point(48, 277);
            this.lblNavBackColor.Name = "lblNavBackColor";
            this.lblNavBackColor.Size = new System.Drawing.Size(135, 21);
            this.lblNavBackColor.TabIndex = 32;
            this.lblNavBackColor.Text = "Background color:";
            // 
            // lblNavbar
            // 
            this.lblNavbar.AutoSize = true;
            this.lblNavbar.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNavbar.Location = new System.Drawing.Point(27, 222);
            this.lblNavbar.Name = "lblNavbar";
            this.lblNavbar.Size = new System.Drawing.Size(182, 28);
            this.lblNavbar.TabIndex = 31;
            this.lblNavbar.Text = "Navbar appearance";
            // 
            // lblSideTextFont
            // 
            this.lblSideTextFont.AutoSize = true;
            this.lblSideTextFont.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSideTextFont.Location = new System.Drawing.Point(48, 168);
            this.lblSideTextFont.Name = "lblSideTextFont";
            this.lblSideTextFont.Size = new System.Drawing.Size(71, 21);
            this.lblSideTextFont.TabIndex = 28;
            this.lblSideTextFont.Text = "Text font:";
            // 
            // btnSideTextColor
            // 
            this.btnSideTextColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSideTextColor.Location = new System.Drawing.Point(518, 116);
            this.btnSideTextColor.Name = "btnSideTextColor";
            this.btnSideTextColor.Size = new System.Drawing.Size(89, 23);
            this.btnSideTextColor.TabIndex = 27;
            this.btnSideTextColor.Text = "Select color";
            this.btnSideTextColor.UseVisualStyleBackColor = true;
            this.btnSideTextColor.Click += new System.EventHandler(this.btnSideTextColor_Click);
            // 
            // tbSideTextColor
            // 
            this.tbSideTextColor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSideTextColor.Location = new System.Drawing.Point(189, 116);
            this.tbSideTextColor.Name = "tbSideTextColor";
            this.tbSideTextColor.Size = new System.Drawing.Size(323, 23);
            this.tbSideTextColor.TabIndex = 26;
            this.tbSideTextColor.Text = "Text color";
            // 
            // lblSideTextColor
            // 
            this.lblSideTextColor.AutoSize = true;
            this.lblSideTextColor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSideTextColor.Location = new System.Drawing.Point(48, 118);
            this.lblSideTextColor.Name = "lblSideTextColor";
            this.lblSideTextColor.Size = new System.Drawing.Size(78, 21);
            this.lblSideTextColor.TabIndex = 25;
            this.lblSideTextColor.Text = "Text color:";
            // 
            // btnSideBackColor
            // 
            this.btnSideBackColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSideBackColor.Location = new System.Drawing.Point(518, 66);
            this.btnSideBackColor.Name = "btnSideBackColor";
            this.btnSideBackColor.Size = new System.Drawing.Size(89, 23);
            this.btnSideBackColor.TabIndex = 24;
            this.btnSideBackColor.Text = "Select color";
            this.btnSideBackColor.UseVisualStyleBackColor = true;
            this.btnSideBackColor.Click += new System.EventHandler(this.btnSideBackColor_Click);
            // 
            // lblSideBackColor
            // 
            this.lblSideBackColor.AutoSize = true;
            this.lblSideBackColor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSideBackColor.Location = new System.Drawing.Point(48, 68);
            this.lblSideBackColor.Name = "lblSideBackColor";
            this.lblSideBackColor.Size = new System.Drawing.Size(135, 21);
            this.lblSideBackColor.TabIndex = 22;
            this.lblSideBackColor.Text = "Background color:";
            // 
            // lblSidebar
            // 
            this.lblSidebar.AutoSize = true;
            this.lblSidebar.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblSidebar.Location = new System.Drawing.Point(27, 13);
            this.lblSidebar.Name = "lblSidebar";
            this.lblSidebar.Size = new System.Drawing.Size(185, 28);
            this.lblSidebar.TabIndex = 21;
            this.lblSidebar.Text = "Sidebar appearance";
            // 
            // AppearanceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 461);
            this.Controls.Add(this.pnlMain);
            this.Name = "AppearanceForm";
            this.Text = "AppearanceForm";
            this.Load += new System.EventHandler(this.AppearanceForm_Load);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblNavTextFont;
        private System.Windows.Forms.Button btnNavTextColor;
        private System.Windows.Forms.TextBox tbNavTextColor;
        private System.Windows.Forms.Label lblNavTextColor;
        private System.Windows.Forms.Button btnNavBackColor;
        private System.Windows.Forms.Label lblNavBackColor;
        private System.Windows.Forms.Label lblNavbar;
        private System.Windows.Forms.Label lblSideTextFont;
        private System.Windows.Forms.Button btnSideTextColor;
        private System.Windows.Forms.TextBox tbSideTextColor;
        private System.Windows.Forms.Label lblSideTextColor;
        private System.Windows.Forms.Button btnSideBackColor;
        private System.Windows.Forms.Label lblSideBackColor;
        private System.Windows.Forms.Label lblSidebar;
        private System.Windows.Forms.ComboBox cbSideFont;
        private System.Windows.Forms.ComboBox cbNavFont;
        private System.Windows.Forms.Panel pnlNavColor;
        private System.Windows.Forms.Panel pnlSideBackColor;
    }
}