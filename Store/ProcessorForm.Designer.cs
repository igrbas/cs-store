﻿
namespace Store {
    partial class ProcessorForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblPrice = new System.Windows.Forms.Label();
            this.tbPrice = new System.Windows.Forms.TextBox();
            this.tbQuantity = new System.Windows.Forms.TextBox();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.tbManufacturer = new System.Windows.Forms.TextBox();
            this.lblManufacturer = new System.Windows.Forms.Label();
            this.lblPicture = new System.Windows.Forms.Label();
            this.tbPicture = new System.Windows.Forms.TextBox();
            this.btnPicture = new System.Windows.Forms.Button();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.btnSaveProcessor = new System.Windows.Forms.Button();
            this.lblCache = new System.Windows.Forms.Label();
            this.tbCache = new System.Windows.Forms.TextBox();
            this.lblCoresNumber = new System.Windows.Forms.Label();
            this.tbCoresNumber = new System.Windows.Forms.TextBox();
            this.lblModel = new System.Windows.Forms.Label();
            this.tbModel = new System.Windows.Forms.TextBox();
            this.lblClockSpeed = new System.Windows.Forms.Label();
            this.tbClockSpeed = new System.Windows.Forms.TextBox();
            this.lblThreadsNumber = new System.Windows.Forms.Label();
            this.tbThreadsNumber = new System.Windows.Forms.TextBox();
            this.ofdPicture = new System.Windows.Forms.OpenFileDialog();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblPrice.Location = new System.Drawing.Point(17, 18);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(58, 28);
            this.lblPrice.TabIndex = 0;
            this.lblPrice.Text = "Price:";
            // 
            // tbPrice
            // 
            this.tbPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPrice.Location = new System.Drawing.Point(180, 23);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Size = new System.Drawing.Size(438, 23);
            this.tbPrice.TabIndex = 1;
            // 
            // tbQuantity
            // 
            this.tbQuantity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbQuantity.Location = new System.Drawing.Point(180, 73);
            this.tbQuantity.Name = "tbQuantity";
            this.tbQuantity.Size = new System.Drawing.Size(438, 23);
            this.tbQuantity.TabIndex = 3;
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblQuantity.Location = new System.Drawing.Point(17, 68);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(97, 28);
            this.lblQuantity.TabIndex = 2;
            this.lblQuantity.Text = "Quantity: ";
            // 
            // tbManufacturer
            // 
            this.tbManufacturer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbManufacturer.Location = new System.Drawing.Point(180, 123);
            this.tbManufacturer.Name = "tbManufacturer";
            this.tbManufacturer.Size = new System.Drawing.Size(438, 23);
            this.tbManufacturer.TabIndex = 5;
            // 
            // lblManufacturer
            // 
            this.lblManufacturer.AutoSize = true;
            this.lblManufacturer.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblManufacturer.Location = new System.Drawing.Point(17, 118);
            this.lblManufacturer.Name = "lblManufacturer";
            this.lblManufacturer.Size = new System.Drawing.Size(133, 28);
            this.lblManufacturer.TabIndex = 4;
            this.lblManufacturer.Text = "Manufacturer:";
            // 
            // lblPicture
            // 
            this.lblPicture.AutoSize = true;
            this.lblPicture.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblPicture.Location = new System.Drawing.Point(17, 168);
            this.lblPicture.Name = "lblPicture";
            this.lblPicture.Size = new System.Drawing.Size(76, 28);
            this.lblPicture.TabIndex = 6;
            this.lblPicture.Text = "Picture:";
            // 
            // tbPicture
            // 
            this.tbPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPicture.Enabled = false;
            this.tbPicture.Location = new System.Drawing.Point(180, 173);
            this.tbPicture.Name = "tbPicture";
            this.tbPicture.Size = new System.Drawing.Size(245, 23);
            this.tbPicture.TabIndex = 7;
            // 
            // btnPicture
            // 
            this.btnPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPicture.Location = new System.Drawing.Point(431, 173);
            this.btnPicture.Name = "btnPicture";
            this.btnPicture.Size = new System.Drawing.Size(187, 23);
            this.btnPicture.TabIndex = 8;
            this.btnPicture.Text = "Select Picture";
            this.btnPicture.UseVisualStyleBackColor = true;
            this.btnPicture.Click += new System.EventHandler(this.btnPicture_Click);
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.btnSaveProcessor);
            this.pnlMain.Controls.Add(this.lblCache);
            this.pnlMain.Controls.Add(this.tbCache);
            this.pnlMain.Controls.Add(this.lblCoresNumber);
            this.pnlMain.Controls.Add(this.tbCoresNumber);
            this.pnlMain.Controls.Add(this.lblModel);
            this.pnlMain.Controls.Add(this.tbModel);
            this.pnlMain.Controls.Add(this.lblClockSpeed);
            this.pnlMain.Controls.Add(this.tbClockSpeed);
            this.pnlMain.Controls.Add(this.lblThreadsNumber);
            this.pnlMain.Controls.Add(this.tbThreadsNumber);
            this.pnlMain.Controls.Add(this.lblPrice);
            this.pnlMain.Controls.Add(this.tbPrice);
            this.pnlMain.Controls.Add(this.btnPicture);
            this.pnlMain.Controls.Add(this.lblQuantity);
            this.pnlMain.Controls.Add(this.tbPicture);
            this.pnlMain.Controls.Add(this.tbQuantity);
            this.pnlMain.Controls.Add(this.lblPicture);
            this.pnlMain.Controls.Add(this.lblManufacturer);
            this.pnlMain.Controls.Add(this.tbManufacturer);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(630, 540);
            this.pnlMain.TabIndex = 11;
            // 
            // btnSaveProcessor
            // 
            this.btnSaveProcessor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveProcessor.Location = new System.Drawing.Point(17, 477);
            this.btnSaveProcessor.Name = "btnSaveProcessor";
            this.btnSaveProcessor.Size = new System.Drawing.Size(601, 23);
            this.btnSaveProcessor.TabIndex = 27;
            this.btnSaveProcessor.Text = "Save";
            this.btnSaveProcessor.UseVisualStyleBackColor = true;
            this.btnSaveProcessor.Click += new System.EventHandler(this.btnSaveProcessor_Click);
            // 
            // lblCache
            // 
            this.lblCache.AutoSize = true;
            this.lblCache.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblCache.Location = new System.Drawing.Point(17, 418);
            this.lblCache.Name = "lblCache";
            this.lblCache.Size = new System.Drawing.Size(68, 28);
            this.lblCache.TabIndex = 25;
            this.lblCache.Text = "Cache:";
            // 
            // tbCache
            // 
            this.tbCache.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCache.Location = new System.Drawing.Point(180, 423);
            this.tbCache.Name = "tbCache";
            this.tbCache.Size = new System.Drawing.Size(438, 23);
            this.tbCache.TabIndex = 26;
            // 
            // lblCoresNumber
            // 
            this.lblCoresNumber.AutoSize = true;
            this.lblCoresNumber.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblCoresNumber.Location = new System.Drawing.Point(17, 368);
            this.lblCoresNumber.Name = "lblCoresNumber";
            this.lblCoresNumber.Size = new System.Drawing.Size(138, 28);
            this.lblCoresNumber.TabIndex = 23;
            this.lblCoresNumber.Text = "Cores number:";
            // 
            // tbCoresNumber
            // 
            this.tbCoresNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCoresNumber.Location = new System.Drawing.Point(180, 373);
            this.tbCoresNumber.Name = "tbCoresNumber";
            this.tbCoresNumber.Size = new System.Drawing.Size(438, 23);
            this.tbCoresNumber.TabIndex = 24;
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblModel.Location = new System.Drawing.Point(17, 218);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(73, 28);
            this.lblModel.TabIndex = 17;
            this.lblModel.Text = "Model:";
            // 
            // tbModel
            // 
            this.tbModel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbModel.Location = new System.Drawing.Point(180, 223);
            this.tbModel.Name = "tbModel";
            this.tbModel.Size = new System.Drawing.Size(438, 23);
            this.tbModel.TabIndex = 18;
            // 
            // lblClockSpeed
            // 
            this.lblClockSpeed.AutoSize = true;
            this.lblClockSpeed.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblClockSpeed.Location = new System.Drawing.Point(17, 268);
            this.lblClockSpeed.Name = "lblClockSpeed";
            this.lblClockSpeed.Size = new System.Drawing.Size(121, 28);
            this.lblClockSpeed.TabIndex = 19;
            this.lblClockSpeed.Text = "Clock speed:";
            // 
            // tbClockSpeed
            // 
            this.tbClockSpeed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbClockSpeed.Location = new System.Drawing.Point(180, 273);
            this.tbClockSpeed.Name = "tbClockSpeed";
            this.tbClockSpeed.Size = new System.Drawing.Size(438, 23);
            this.tbClockSpeed.TabIndex = 20;
            // 
            // lblThreadsNumber
            // 
            this.lblThreadsNumber.AutoSize = true;
            this.lblThreadsNumber.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblThreadsNumber.Location = new System.Drawing.Point(17, 318);
            this.lblThreadsNumber.Name = "lblThreadsNumber";
            this.lblThreadsNumber.Size = new System.Drawing.Size(157, 28);
            this.lblThreadsNumber.TabIndex = 21;
            this.lblThreadsNumber.Text = "Threads number:";
            // 
            // tbThreadsNumber
            // 
            this.tbThreadsNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbThreadsNumber.Location = new System.Drawing.Point(180, 323);
            this.tbThreadsNumber.Name = "tbThreadsNumber";
            this.tbThreadsNumber.Size = new System.Drawing.Size(438, 23);
            this.tbThreadsNumber.TabIndex = 22;
            // 
            // ProcessorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 540);
            this.Controls.Add(this.pnlMain);
            this.KeyPreview = true;
            this.Name = "ProcessorForm";
            this.Text = "Processor form";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ProcessorForm_KeyDown);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox tbPrice;
        private System.Windows.Forms.TextBox tbQuantity;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.TextBox tbManufacturer;
        private System.Windows.Forms.Label lblManufacturer;
        private System.Windows.Forms.Label lblPicture;
        private System.Windows.Forms.TextBox tbPicture;
        private System.Windows.Forms.Button btnPicture;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Button btnSaveProcessor;
        private System.Windows.Forms.Label lblCache;
        private System.Windows.Forms.TextBox tbCache;
        private System.Windows.Forms.Label lblCoresNumber;
        private System.Windows.Forms.TextBox tbCoresNumber;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.Label lblClockSpeed;
        private System.Windows.Forms.TextBox tbClockSpeed;
        private System.Windows.Forms.Label lblThreadsNumber;
        private System.Windows.Forms.TextBox tbThreadsNumber;
        private System.Windows.Forms.OpenFileDialog ofdPicture;
    }
}